package com.example.musicapp

import com.example.musicapp.Song
import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.provider.Settings
import android.view.View
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import java.io.IOException

class MainActivity : AppCompatActivity() {

    private val mediaPlayer = MediaPlayer()
    private val musicList = mutableListOf<String>()
    private var current = 0
    private lateinit var seekBar: SeekBar
    private val handler = Handler(Looper.getMainLooper())

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        seekBar = findViewById(R.id.seekBar)

        // 检查并请求权限
        checkPermissions()

        // 设置 SeekBar 的拖动监听
        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    mediaPlayer.seekTo(progress) // 更新 MediaPlayer 的播放位置
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                // 暂停音乐
                if (mediaPlayer.isPlaying) {
                    mediaPlayer.pause()
                }
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                // 恢复音乐
                mediaPlayer.start()
            }
        })

    }

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    private fun checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_MEDIA_AUDIO), 0)
        } else {
            loadMusic()
        }
    }

    private fun loadMusic() {
        val projection = arrayOf(MediaStore.Audio.Media.DATA)
        val cursor = contentResolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, projection, null, null, null)
        cursor?.use {
            val dataColumn = it.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA)
            while (it.moveToNext()) {
                val path = it.getString(dataColumn)
                musicList.add(path)
            }
        }
        if (musicList.isEmpty()) {
            Toast.makeText(this, "未找到音乐文件", Toast.LENGTH_SHORT).show()
        }
    }

    fun onPlay(v: View) {
        if (musicList.isNotEmpty()) {
            try {
                mediaPlayer.reset()
                mediaPlayer.setDataSource(musicList[current])
                mediaPlayer.prepare()
                mediaPlayer.start()
                seekBar.max = mediaPlayer.duration

                mediaPlayer.setOnCompletionListener {
                    onNext(v) // 自动播放下一首
                }

                updateSeekBar()
            } catch (e: IOException) {
                e.printStackTrace()
                Toast.makeText(this, "播放出错: ${e.message}", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(this, "音乐列表为空", Toast.LENGTH_SHORT).show()
        }
    }

    private fun updateSeekBar() {
        handler.post(object : Runnable {
            override fun run() {
                if (mediaPlayer.isPlaying) {
                    seekBar.progress = mediaPlayer.currentPosition
                    handler.postDelayed(this, 1000) // 每秒更新一次
                }
            }
        })
    }

    fun onPause(v: View) {
        if (mediaPlayer.isPlaying) {
            mediaPlayer.pause() // 暂停
        }
    }

    fun onStop(v: View) {
        mediaPlayer.stop() // 停止播放
        mediaPlayer.reset()
        handler.removeCallbacksAndMessages(null) // 移除所有更新 SeekBar 的回调
        finish() // 退出应用
    }

    fun onNext(v: View) {
        if (musicList.isNotEmpty()) {
            current = (current + 1) % musicList.size // 循环到下一首
            onPlay(v) // 播放下一首
        } else {
            Toast.makeText(this, "音乐列表为空", Toast.LENGTH_SHORT).show()
        }
    }

    fun onPrev(v: View) {
        if (musicList.isNotEmpty()) {
            current = (current - 1 + musicList.size) % musicList.size // 循环到上一首
            onPlay(v) // 播放上一首
        } else {
            Toast.makeText(this, "音乐列表为空", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 0) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                loadMusic()
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_AUDIO)) {
                    Toast.makeText(this, "需要读取音频文件的权限", Toast.LENGTH_SHORT).show()
                }
                showPermissionDeniedDialog()
            }
        }
    }

    private fun showPermissionDeniedDialog() {
        AlertDialog.Builder(this)
            .setTitle("权限被拒绝")
            .setMessage("为了正常使用本应用，您需要允许读取存储权限。请前往设置手动开启。")
            .setPositiveButton("去设置") { _, _ ->
                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                val uri = Uri.fromParts("package", packageName, null)
                intent.data = uri
                startActivity(intent)
            }
            .setNegativeButton("取消", null)
            .show()
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer.release() // 释放资源
        handler.removeCallbacksAndMessages(null) // 清除所有的处理器回调
    }
}

package com.example.musicapp

// 定义一个数据类 Song，用于存储歌曲的标题和艺术家
data class Song(val title: String, val artist: String)
